var express = require('express');
var app = express();

var items = require('./routes/items');
var users = require('./routes/users');
var licenses = require('./routes/licenses');
var branches = require('./routes/branches');

var bodyParser = require('body-parser');
var cors = require('express-cors');

// Routers
app.use('/items', items);
app.use('/users', users);
app.use('/licenses', licenses);
app.use('/branches', branches);

app.listen(3000);
