var express = require('express');
var router = express.Router();

var pool = require('../db/config');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({extend: false});


// Update that dirty stuff plz

router.post('/new', jsonParser, function(req,res) {

    pool.getConnection(function(err, connection) {
        if(err) {
            res.json({"code": 100, "status": err});
            return;
        } else {

            var license = connection.escape(req.body.license);
            var creator = connection.escape(req.body.creator);
            var master = connection.escape(req.body.master);
            var open = connection.escape(req.body.public);

            var sql = "INSERT INTO Branche (license_id, creator, master, uptodate, public, active) VALUES "+
            "("+license+", "+creator+", "+master+", true, "+open+", true)";

            connection.query(sql, function(err, result) {
                if(err) {

                    res.json({"code": 500, "status": err});
                    return

                } else {

                    connection.release();
                    res.json({"code": 100, "status": "Branche créée"});

                }
            });
        }
    });

});

router.post('/duplicate/:master/', jsonParser, function(req, res) {

    pool.getConnection(function(err, connection) {
        if(err) {
            res.json({"code": 100, "status": err});
            return;
        } else {

            var creator = connection.escape(req.body.creator);
            var open = connection.escape(req.body.open);
            var master = connection.escape(req.params.master);

            var sql = "INSERT INTO Branche (creator, master, public) VALUES "+
            "("+creator+", "+master+", "+open+")";

            connection.query(sql, function(err, result) {

                if(err) {

                    res.json({"code": 500, "status": err});
                    return

                } else {

                    connection.release();
                    res.json({"state": "Branche "+master+" dupliquée"});

                }

            });

        }
    })

});

router.post('/synchronize/:branche', function(req, res) {

    var branche_id = req.params.branche;
    pool.getConnection(function(err, connection) {

      if(err) {
        res.json({"code": 500, "status": err});
        return;
      } else {

        // Get master id
          var query = "SELECT master FROM Branche WHERE branche_id = "+branche_id+" LIMIT 1";

          connection.query(query, function(err, rows) {
            connection.release();

            compare_branches(branche_id, rows[0].master, function(items) {
              synchronize_branche(branche_id, items, function(result) {

                if(result.success) {

                  set_uptodate(branche_id, true, function(result) {
                    res.json(result);
                  });

                } else {
                  res.json(result);
                }

              });
            });
          });
      }
    });
});

router.post('/disable/:branche_id', function(req, res) {

  var branche_id = req.params.branche_id;
  pool.getConnection(function(err, connection) {

    var query = "UPDATE Branche SET active = 0 WHERE branche_id = "+branche_id+"";
    connection.query(query, function(err, result) {

      if(err) {

        res.json({"err": err});
        return;

      } else {

        res.json({"success":true});

      }
    });
  });

});



function set_uptodate(branche_id, state, callback) {

  var query = "UPDATE Branche SET uptodate = "+state+" WHERE branche_id = "+branche_id+"";
  pool.getConnection(function(err, connection) {

    connection.query(query, function(err, result) {
      if(err) {

        callback({"success": false});

      } else {

        callback({"success": true});

      }
    });
  });
}


function synchronize_branche(branche_id, items, callback) {

  pool.getConnection(function(err, connection) {
    if(err) {

      res.json({"code": 500, "status": err});
      return;

    } else {

      var sql = "INSERT INTO Item(branche_id, title, type, item_number, spinoff) VALUES ";

      for(var i=0; i<items.length; i++) {

          if(i !== 0) {
              sql += ", "
          }

          sql += "("+connection.escape(branche_id)+
              ","+connection.escape(items[i].title)+
              ","+connection.escape(items[i].type)+
              ","+connection.escape(items[i].item_number)+
              ","+connection.escape(items[i].spinoff)+")";

      }

      sql += ";";
      connection.query(sql, function(err, result) {
        if(err) {
          callback({"success":false});
        } else {

          connection.release();
          callback({"success":true})

        }
      });
    }
  });
}


function compare_branches(branche_id, master_id, callback) {

  pool.getConnection(function(err, connection) {
    if(err) {

      res.json({"code":500, "status": err});
      return;

    } else {

      var query = ""+
                    "SELECT v.title, v.type, v.item_number, v.spinoff FROM "
          +"(SELECT DISTINCT branche_id AS masta, title, type, item_number, spinoff FROM Item WHERE branche_id = "+master_id+") v"
          +"  LEFT JOIN (SELECT DISTINCT branche_id AS slava, title, type, item_number, spinoff FROM Item WHERE branche_id = "+branche_id+") w"
          +"  ON w.title = v.title AND w.type = v.type AND w.item_number = v.item_number AND w.spinoff = v.spinoff"
          +"  WHERE slava IS NULL";

      connection.query(query, function(err, diffs) {

        if(err) {
          return {"code": 500, "status": err};
        } else {

          connection.release();
          callback(diffs);

        }
      });
    }
  });

}
module.exports = router;
