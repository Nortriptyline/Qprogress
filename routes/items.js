var express = require('express');
var router = express.Router();

var pool = require('../db/config');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({extend: false});

var Branche = require('../objects/Branche');
var Extra = require('../Extend');


// Store new item(s) in a defined branche
router.post('/new/:branche_id',jsonParser, function(req, res) {

    pool.getConnection(function(err, connection) {

        if(err) {
            res.json({"error": err});
            return
        } else {

          var branche = new Branche(req.params.branche_id);
          branche.isActive(function(result) {

            if(!result.errCode) {

              if(result.res) {

                Extra.parseItems(req.params.branche_id, req.body, function(items) {
                  branche.insertItems(items, function(result) {

                    res.json(result);

                  });

                });

              } else {
                // Branche found but is inactive
                res.json({"state": 302, "message": "Branche is inactive"});
              }

            } else {
              // An error occured
              res.json({"state": result.errCode, "message": result.res});
            }

          });
        }

    });

});

module.exports = router;
