var express = require('express');
var router = express.Router();

var pool = require('../db/config');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({extend: false})

router.get('/', function(req, res) {
    pool.getConnection(function(err, connection) {

        if(err){
            res.json({"code": 100, "status": err});
            return;
        } else {

            connection.on('error', function(err) {
                res.json({"code": 101, "status": "Database connection error"});
                return;
            });

            connection.release();
            res.json({"id": connection.threadId});
        }


    });
});

router.post('/new', jsonParser, function(req,res) {

    pool.getConnection(function(err, connection) {
        if(err) {
            res.json({"code": 100, "status": err});
            return;
        } else {

            var username = connection.escape(req.body.username);
            var sql = "INSERT INTO User (username) VALUES ("+username+")";

            connection.query(sql, function(err, result) {
                if(err) {
                    res.json({"code": 500, "status": err});
                    return
                } else {

                    connection.release();
                    res.json({"code": 100, "status": "Utilisateur enregistré"});

                }
            });

        }
    })

});

module.exports = router;
