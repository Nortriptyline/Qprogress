-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2017 at 08:16 AM
-- Server version: 5.7.16-0ubuntu0.16.10.1
-- PHP Version: 7.0.8-3ubuntu3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Qprogress`
--

-- --------------------------------------------------------

--
-- Table structure for table `Branche`
--

CREATE TABLE `Branche` (
  `branche_id` int(10) UNSIGNED NOT NULL,
  `license_id` int(10) UNSIGNED DEFAULT NULL,
  `creator` int(10) UNSIGNED NOT NULL,
  `master` int(10) UNSIGNED DEFAULT NULL,
  `uptodate` tinyint(1) NOT NULL DEFAULT '1',
  `public` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Branche`
--

INSERT INTO `Branche` (`branche_id`, `license_id`, `creator`, `master`, `uptodate`, `public`, `active`) VALUES
(1, 18, 1, NULL, 1, 1, 1),
(2, NULL, 1, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Finished`
--

CREATE TABLE `Finished` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `finished` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Item`
--

CREATE TABLE `Item` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `branche_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) NOT NULL,
  `type` enum('book','series','film','game') NOT NULL,
  `item_number` tinyint(3) UNSIGNED DEFAULT NULL,
  `spinoff` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Item`
--

INSERT INTO `Item` (`item_id`, `branche_id`, `title`, `type`, `item_number`, `spinoff`) VALUES
(10, 1, 'Episode 1', 'series', 1, 0),
(11, 2, 'Episode 1', 'series', 1, 0),
(12, 1, 'Episode 2', 'series', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `License`
--

CREATE TABLE `License` (
  `license_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `License`
--

INSERT INTO `License` (`license_id`, `name`) VALUES
(18, 'Black Lagoon');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`user_id`, `username`) VALUES
(1, 'nortri');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Branche`
--
ALTER TABLE `Branche`
  ADD PRIMARY KEY (`branche_id`),
  ADD UNIQUE KEY `mono_branche_license` (`license_id`,`creator`),
  ADD KEY `creator` (`creator`);

--
-- Indexes for table `Finished`
--
ALTER TABLE `Finished`
  ADD PRIMARY KEY (`item_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Item`
--
ALTER TABLE `Item`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `item_branche_fk` (`branche_id`);

--
-- Indexes for table `License`
--
ALTER TABLE `License`
  ADD PRIMARY KEY (`license_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Branche`
--
ALTER TABLE `Branche`
  MODIFY `branche_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Item`
--
ALTER TABLE `Item`
  MODIFY `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `License`
--
ALTER TABLE `License`
  MODIFY `license_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Branche`
--
ALTER TABLE `Branche`
  ADD CONSTRAINT `Branche_ibfk_1` FOREIGN KEY (`license_id`) REFERENCES `License` (`license_id`),
  ADD CONSTRAINT `Branche_ibfk_2` FOREIGN KEY (`creator`) REFERENCES `User` (`user_id`);

--
-- Constraints for table `Finished`
--
ALTER TABLE `Finished`
  ADD CONSTRAINT `Finished_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `Item` (`item_id`),
  ADD CONSTRAINT `Finished_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `User` (`user_id`);

--
-- Constraints for table `Item`
--
ALTER TABLE `Item`
  ADD CONSTRAINT `item_branche_fk` FOREIGN KEY (`branche_id`) REFERENCES `Branche` (`branche_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
