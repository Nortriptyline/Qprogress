var Item = require('./objects/Item');

function Extra() {}

Extra.prototype.parseItems = function(branche, items, callback) {
  var result = [];

  if(branche) {

    for(var i=0; i<items.length; i++) {

      var newItem = new Item(branche, items[i].title, items[i].type, items[i].number, items[i].spinoff);
      result.push(newItem);
    }

  } else {

  }

  callback(result);

}

module.exports = new Extra();
