CREATE TABLE Follow(
  link_id INT NOT NULL AUTO_INCREMENT,
  user INT NOT NULL,
  branche INT NOT NULL,
  active boolean NOT NULL,
  PRIMARY KEY (link_id),
  UNIQUE(user, branche),
  FOREIGN KEY(user) REFERENCES User(user_id),
  FOREIGN KEY(branche) REFERENCES Branche(branche_id)
);
