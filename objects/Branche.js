var pool = require('../db/config');

function Branche(id) {
  this.id = id;
}

Branche.prototype.isActive = function(callback) {

  var id = this.id;

  pool.getConnection(function(err, connection) {

    if(err) {
      console.log('error : '+ err);
      return;
    } else {

      var sql = "SELECT active FROM Branche WHERE branche_id = "+connection.escape(id)+" LIMIT 1";

      connection.query(sql, function(err, result) {

        connection.release();
        if(err){
          callback({"errCode": 500, "res" : err});
        } else {

          if(result.length <= 0){

            callback({"errCode": 404, "res": "Branche not found"})

          } else {

            callback({"res" : result[0].active});

          }

        }
      });
    }
  });
}

Branche.prototype.insertItems = function(items, callback) {
  pool.getConnection(function(err, connection) {

    if(err) {
      console.log('error : '+ err);
      return;
    } else {

      var sql = "INSERT INTO Item(branche_id, title, type, item_number, spinoff) VALUES ";

      for(var i=0; i<items.length; i++) {

          if(i !== 0) {
              sql += ", "
          }

          sql += "("+connection.escape(items[i].branche)+
            ","+connection.escape(items[i].title)+
            ","+connection.escape(items[i].type)+
            ","+connection.escape(items[i].item_number)+
            ","+connection.escape(items[i].spinoff)+
            ")";

        }

        sql += ";";

      connection.query(sql, function(err, result) {
        if(err) {
          connection.release();
          callback({"errCode" : err.errno, "message":err.code})
        } else {
          connection.release();

          //Turn branches out of date
          callback({"res": "Items enregistrés"});

        }
      })
    }
  });
}

module.exports = Branche;
