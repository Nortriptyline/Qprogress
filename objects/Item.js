function Item(branche, title, type, number, spinoff) {

  this.branche = branche;
  this.title = title;
  this.type = type;
  this.number = number;
  this.spinoff = spinoff;

}

module.exports = Item;
