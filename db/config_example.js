// Rename this file config.js for use
var mysql = require('mysql');
var pool = mysql.createPool({

    host: 'db_uri',
    user: 'db_user',
    password: 'db_password',
    database: 'db_name'

});

module.exports = pool;
